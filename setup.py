from setuptools import find_packages, setup


def read(filename):
    with open(filename) as f:
        return f.read()


setup(
    name='stonestyle',
    version='0.1.0',
    author='Andy Dirnberger',
    author_email='andy@dirnberger.me',
    url='https://gitlab.com/dirn/stonestyle',
    description='A linter for Stone files',
    long_description=read('README.rst'),
    license='MIT',
    packages=find_packages(),
    python_requires='>=3.6',
    install_requires=[
        # TODO: Set this to '>0.1' once a new release of Stone is
        # available. This linter requires the package structure on
        # GitHub, but that isn't available on PyPI.
        'stone',
    ],
    entry_points={
        'console_scripts': [
            'stonestyle = stonestyle.cli:main',
        ],
    },
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Console',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3 :: Only',
        'Topic :: Software Development :: Libraries :: Python Modules',
    ]
)
