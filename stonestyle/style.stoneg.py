"""A linter for Stone files."""

import argparse
import re

from stone.backend import CodeBackend
from stone.frontend.ast import AstRouteDef
from stone.ir import Api, ApiNamespace

_parser = argparse.ArgumentParser(prog='linter.stonestyle')

CAMEL_CASE_PATTERN = re.compile('([A-Z][a-z0-9]+)+$')
SNAKE_CASE_PATTERN = re.compile('[a-z]+(_[a-z]+)*$')


class StoneLinter(CodeBackend):
    """Provide formatting feedback for one or more Stone files."""

    preserve_aliases = True
    cmdline_parser = _parser

    def generate(self, api: Api) -> None:
        """Call the linting functions."""
        for namespace in api.namespaces.values():
            camel_case_names(namespace)
            snake_case_names(namespace)


def camel_case_names(namespace: ApiNamespace) -> None:
    """Check that unions, structs, and aliases use camel case."""
    unions_and_structs = namespace.data_types
    aliases = namespace.aliases

    for definition in unions_and_structs + aliases:
        if not CAMEL_CASE_PATTERN.match(definition.name):
            emit(
                definition._ast_node,
                'Union and struct names should be camel case',
            )


def emit(token: AstRouteDef, message: str) -> None:
    """Emit a warning message."""
    print(f'{token.path}:{token.lineno}: warning: {message}')


def snake_case_names(namespace: ApiNamespace) -> None:
    """Check that fields and routes use camel case."""
    unions_and_structs = namespace.data_types
    routes = namespace.routes

    for definition in unions_and_structs:
        for field in definition.fields:
            if not SNAKE_CASE_PATTERN.match(field.name):
                emit(
                    field._ast_node,
                    'Union and struct field names should be snake case',
                )

    for definition in routes:
        if not SNAKE_CASE_PATTERN.match(definition.name):
            emit(
                definition._ast_node,
                'Route names should be snake case',
            )
