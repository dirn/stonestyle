"""stonestyle's command line interface."""

from contextlib import redirect_stderr
from glob import glob
import os.path
import sys

import stone.cli


def main():
    """Run the command line interface."""
    entrypoint, *files = sys.argv
    if not files:
        files = glob(os.path.join('**', '*.stone'), recursive=True)

    here = os.path.abspath(os.path.dirname(__file__))
    linter = os.path.join(here, 'style.stoneg.py')

    # Stone expects to be invoked as `stone <backend> <output> <input>`.
    # If not input files are given, Stone will read from stdin. Here we
    # tell Stone to use the linter as the backend, the working directory
    # as the output location (the location doesn't matter, but /dev/null
    # can't be used because it isn't a folder), and either the files
    # specified when stonestyle was invoked or all of the Stone files
    # found in the working directory.
    sys.argv = [entrypoint, linter, os.getcwd()] + files

    # Stone sends syntax errors to stdout but we want all errors and
    # warnings to go to stdout instead.
    with redirect_stderr(sys.stdout):
        stone.cli.main()
